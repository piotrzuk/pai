<?php

require_once("AppController.php");
require_once __DIR__.'/../model/Phone.php';
require_once __DIR__.'/../model/PhoneMapper.php';

class PhoneController extends AppController{
    public function phoneList(){
        $mapper = new phoneMapper();

            $phones = $mapper ->getPhones();
            // $employee = $mapper ->getEmployee();
            $this->render('phonesList', ['phones' => $phones]);
    }

    public function phoneAdd(){
        $mapper = new PhoneMapper();
        $phone = null;

        if($this -> isPost()){
         $phone = new Phone ($_POST['imei'], $_POST['phone_number'], $_POST['phone_model'], $_POST['status'], $_POST['id_employee']);

        $mapper -> setPhone($phone);

        $this->redirect('?page=phoneList');
        }
        else{
        $mapper = new PhoneMapper();
        $employess = $mapper ->getEmployess();
        $this->render('phoneAdd',['employess' => $employess]);
        }
    }

    public function phoneDelete(): void{
        if (!isset($_POST['id_phone'])) {
            http_response_code(404);
            return;
        }

        $phone = new PhoneMapper();
        $phone->delete((int)$_POST['id_phone']);

        http_response_code(200);
    }

}
