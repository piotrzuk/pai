<?php

require_once("AppController.php");
require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';


class DefaultController extends AppController{
    public function home(){
        $text = 'Strona startowa';

        $this->render('home', ['text' => $text]);
    }

    public function login(){
        $mapper = new UserMapper();

        $user = null;

        if ($this->isPost()) {

            $user = $mapper->getUser($_POST['login']);

            if(!$user) {
                return $this->render('login', ['message' => ['Błędny login']]);
            }

            if ($user->getPassword() !== $_POST['password']) {
                return $this->render('login', ['message' => ['Złe hasło']]);
            } else {
                $_SESSION["id"] = $user->getLogin();
                $_SESSION["role"] = $user->getRole();
                header("Location: ?page=home");

                exit();
            }
        }

        $this->render('login');
    }

    public function logout(){
        session_unset();
        session_destroy();
        //TODO Sprawdzić jak to wyglada
        $this->render('login', ['text' => 'Zostałeś wylogowan']);
    }

    public function register()
    {

        $this->render('register');
    }
}

