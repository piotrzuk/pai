<?php

require_once("AppController.php");
require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';

class AdminController extends AppController{
    public function index(): void{
        $user = new UserMapper();
        $this->render('index', ['user' => $user -> getUser($_SESSION['id'])]);
    }

    public function users(): void{
        $user = new UserMapper();

        header('Content-type: application/json');
        http_response_code(200);

        echo $user->getUsers() ? json_encode($user->getUsers()) : '';
    }

    public function userAdd(){
        $mapper = new UserMapper();
        $user = null;

        if($this -> isPost()){
            $user = new User ($_POST['login'], $_POST['email'], $_POST['password'], $_POST['id_role']);
            $mapper -> setUser($user);
            $user = new UserMapper();
            $this->redirect('?page=admin');
        }
        else{
            $mapper = new UserMapper();
            $roles = $mapper -> getRoles();
            $this->render('addUser',['roles' => $roles]);
        }
    }

    public function userDelete(): void {
        if (!isset($_POST['id_user'])) {
            http_response_code(404);
            return;
        }

        $user = new UserMapper();
        $user->delete((int)$_POST['id_user']);

        http_response_code(200);
    }

}