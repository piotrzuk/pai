<?php

require_once("AppController.php");
require_once __DIR__.'/../model/Employee.php';
require_once __DIR__.'/../model/EmployeeMapper.php';

class EmployeeController extends AppController{
    public function employeeList(){
        $mapper = new EmployeeMapper();

            $employee = $mapper ->getEmployess();

            $this->render('employeeList', ['employess' => $employee]);
    }

    public function employeeAdd(){
        $mapper = new EmployeeMapper();
        $employee = null;

        if($this -> isPost()){
         $employee = new Employee ($_POST['name'], $_POST['surname']);

        $mapper -> setEmployee($employee);

        $this->redirect('?page=employeeList');
        }
        else{
        $mapper = new EmployeeMapper();
        $employess = $mapper ->getEmployess();
        $this->render('employeeAdd',['employee' => $employee]);
        }
    }

    public function employeeDelete(): void{
        if (!isset($_POST['id_employee'])) {
            http_response_code(404);
            return;
        }

        $employee = new EmployeeMapper();
        $employee->delete((int)$_POST['id_employee']);

        http_response_code(200);
    }

}