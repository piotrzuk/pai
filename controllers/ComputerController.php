<?php

require_once("AppController.php");
require_once __DIR__.'/../model/Computer.php';
require_once __DIR__.'/../model/ComputerMapper.php';

class ComputerController extends AppController{
    public function computerLst(){
        $mapper = new ComputerMapper();

            $computer = $mapper ->getComputers();
            $employee = $mapper ->getEmployess();
            $this->render('computerList', ['computers' => $computer, 'employess' => $employee]);
    }

    public function computerAdd(){
        $mapper = new ComputerMapper();
        $computer = null;

        if($this -> isPost()){
         $computer = new Computer ($_POST['computer_name'], $_POST['computer_model'], $_POST['service_tag'], $_POST['status'], $_POST['id_employee']);

        $mapper -> setComputer($computer);

        $this->redirect('?page=computerList');
        }
        else{
        $mapper = new ComputerMapper();
        $employess = $mapper ->getEmployess();
        $this->render('computerAdd',['employess' => $employess]);
        }
    }

    public function computerDelete(): void{
        if (!isset($_POST['id_computer'])) {
            http_response_code(404);
            return;
        }

        $computer = new ComputerMapper();
        $computer->delete((int)$_POST['id_computer']);

        http_response_code(200);
    }

}