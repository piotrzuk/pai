<?php

require_once('controllers/DefaultController.php');
require_once('controllers/ComputerController.php');
require_once('controllers/AdminController.php');
require_once('controllers/PhoneController.php');
require_once('controllers/EmployeeController.php');

class Routing
{
    public $routes = [];

    public function __construct()
    {
        $this->routes = [
            'index' => [
                'controller' => 'DefaultController',
                'action' => 'index'
            ],
            'login' => [
                'controller' => 'DefaultController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'DefaultController',
                'action' => 'logout'
            ],

            'home' => [
                'controller' => 'DefaultController',
                'action' => 'home'
            ],
            'computerList' => [
                'controller' => 'ComputerController',
                'action' => 'computerLst'
            ],
            
            'computerAdd' => [
                'controller' => 'ComputerController',
                'action' => 'computerAdd'
            ],
            'computerDelete' => [
                'controller' => 'ComputerController',
                'action' => 'computerDelete'
            ],

            'admin' => [
                'controller' => 'AdminController',
                'action' => 'index',

            ],
            'adminUsers' => [
                'controller' => 'AdminController',
                'action' => 'users'
                // TODO dodane działającego requie role
                // 'requiredRole' => 1
            ],
            'adminDeleteUsers' => [
                'controller' => 'AdminController',
                'action' => 'userDelete',
                // 'requiredRole' => 1
            ],
            'addUsers' => [
                'controller' => 'AdminController',
                'action' => 'userAdd'
            ],
            'phoneList' => [
                'controller' => 'PhoneController',
                'action' => 'phoneList'
            ],
            
            'phoneAdd' => [
                'controller' => 'PhoneController',
                'action' => 'phoneAdd'
            ],
            'phoneDelete' => [
                'controller' => 'PhoneController',
                'action' => 'phoneDelete'
            ],
            'employeeList' => [
                'controller' => 'EmployeeController',
                'action' => 'employeeList'
            ],
            
            'employeeAdd' => [
                'controller' => 'EmployeeController',
                'action' => 'employeeAdd'
            ],
            'employeeDelete' => [
                'controller' => 'EmployeeController',
                'action' => 'employeeDelete'
            ]
        ];
    }
       
    public function run(){
        $page = isset($_GET['page'])
            && isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'login';
    
        if ($this->routes[$page]) {
            $class = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $requiredRole = $this->routes[$page]['requiredRole'] ?? null;

            if ($requiredRole != null && $requiredRole != $_SESSION['role']) {
                header('Location: /?page=login');
            }
            $object = new $class;
            $object->$action();
        }
    }
}