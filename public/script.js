function getUsers() {
    const apiUrl = "http://localhost/pai/pai";
    const $list = $('.users-list');

    $.ajax({
        url : apiUrl + '/?page=adminUsers',
        dataType : 'json'
    })
        .done((res) => {

            $list.empty();
            res.forEach(el => {
                $list.append(`<tr>
                    <td>${el.login}</td>
                    <td>${el.email}</td>
                    <td>${el.rola}</td>
                    <td>
                    <button class="btn btn-danger" type="button" onclick="deleteUser(${el.id_user})">
                        <i class="material-icons">delete_forever</i>
                    </button>
                    </td>
                    </tr>`);
            })
        });
}

function deleteUser(id_user){
    if(!confirm('Czy na pewno chcesz usunąć konto?')){
        return;
    }

    const apiUrl = "http://localhost/pai/pai";
    $.ajax({
        url : apiUrl + '/?page=adminDeleteUsers',
        method : "POST",
        data : {
            id_user : id_user
        },
        success: function() {
            alert('Usunięto użytkowinka');
            getUsers();
        }
    });
}

function deleteComputer(id_computer){
    if(!confirm('Czy na pewno chcesz usunąć komputer?')){
        return;
    }

    const apiUrl = "http://localhost/pai/pai";
    $.ajax({
        url : apiUrl + '/?page=computerDelete',
        method : "POST",
        data : {
            id_computer : id_computer
        },
        success: function() {
            alert('Komputer został usunięty');
            window.location.href = '?page=computerList';
        }
    });
}

function deletePhone(id_phone){
    if(!confirm('Czy na pewno chcesz usunąć telefon?')){
        return;
    }
    console.log('hello' + id_phone)

    const apiUrl = "http://localhost/pai/pai";
    $.ajax({
        url : apiUrl + '/?page=phoneDelete',
        method : "POST",
        data : {
            id_phone : id_phone
        },
        success: function() {
            alert('telefon został usunięty');
            window.location.href = '?page=phoneList';
        }
    });
}

function deleteEmployee(id_employee){
    if(!confirm('Czy na pewno chcesz usunąć pracownika?')){
        return;
    }

    const apiUrl = "http://localhost/pai/pai";
    $.ajax({
        url : apiUrl + '/?page=employeeDelete',
        method : "POST",
        data : {
            id_employee : id_employee
        },
        success: function() {
            alert('Pracownik został usunięty');
            window.location.href = '?page=employeeList';
        }
    });
}
