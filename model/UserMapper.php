<?php

require_once 'User.php';
require_once __DIR__.'/../Database.php';

class UserMapper{
    private $database;

    public function __construct(){
        $this->database = new Database();
    }

    public function getUser(
        string $login
    ):User {
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM users  LEFT JOIN role ON users.id_role = role.id_role WHERE login = :login ;');
            $stmt->bindParam(':login', $login, PDO::PARAM_STR);
            $stmt->execute();

            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            $newUser = new User($user['login'], $user['email'], $user['password'], $user['id_role']);

            $newUser->setRole($user['rola']);
            return $newUser;

        }
        catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    public function getUsers(){
        try {
            $stmt = $this->database->connect()->prepare('SELECT users.*, role.*, role.id_role FROM users LEFT JOIN role ON users.id_role = role.id_role WHERE login != :login;');
            $stmt->bindParam(':login', $_SESSION['id'], PDO::PARAM_STR);
            $stmt->execute();

            $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $user;
        }
        catch(PDOException $e) {
            die();
        }
    }
    public function getRoles(){
        $sql = 'SELECT * FROM role';
        
        $stmt = $this->database->connect()->prepare($sql);
        $stmt->execute();
        $roles = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $roles;
    }

    public function setUser(User $user) :void{
        $login = $user->getLogin();
        $email = $user->getEmail();
        $password = $user->getPassword();
        $id_role = $user->getId_Role();
// TODO Haszowanie z zasoleniem hasła

        try{
            $stmt = $this->database->connect()->prepare('INSERT INTO users (login, email, password, id_role) VALUES (:login, :email, :password, :id_role);');
                
            $stmt->bindParam(':login', $login, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':password', $password, PDO::PARAM_STR);
            $stmt->bindParam(':id_role', $id_role, PDO::PARAM_STR);
            $stmt->execute();
        }

        catch(PDOException $e) {
            die();
        }
    }

    public function delete(int $id_user): void{
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM users WHERE id_user = :id_user;');
            $stmt->bindParam(':id_user', $id_user, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $e) {
            die();
        }
    }
}