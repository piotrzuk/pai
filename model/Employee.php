<?php

class Employee{
    private $id_employee;
    private $name;
    private $surename;

    public function __construct($name, $surename){
        $this->name = $name;
        $this->surename = $surename;
    }

    public function getId_employee(){
        return $this->id_employee;
    }

    public function setId_employee($id_employee): void{
        $this->id_employee = $id_employee;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name): void{
        $this->name = $name;
    }

    public function getSurename(){
        return $this->surename;
    }

    public function setSurename($surename): void{
        $this->surename = $surename;
    }
}