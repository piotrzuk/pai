<?php

class User{
    private $id_user;
    private $login;
    private $email;
    private $password;
    private $enable;
    private $id_role;
    private $role;

    public function __construct($login, $email, $password, $id_role){
        $this->login = $login;
        $this->email = $email;
        $this->password = $password;
        $this->id_role = $id_role;
    }
    public function getRole(){
        return $this->role;
    }

    public function setRole($role): void{
        $this->role = $role;
    }
   

    public function getLogin(){
        return $this->login;
    }

    public function setLogin($login): void{
        $this->login = $login;
    }
   
    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email): void{
        $this->email = $email;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password): void{
        $this->password = $password;
    }

    public function setId_user($id_user): void{
        $this->id_user = $id_user;
    }

    public function getId_user(){
        return $this->$id_user;
    }

    public function getId_Role(){
        return $this->id_role;
    }

    public function setId_Role(string $id_role): void{
        $this->id_role = $id_role;
    }

    public function getEnable(): string{
        return $this->enable;
    }

    public function setEnable(string $enable): void{
        $this->enable = $enable;
    }
}