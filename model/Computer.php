<?php

class Computer{
    private $id_computer;
    private $computer_name;
    private $computer_model;
    private $service_tag;
    private $status;
    private $id_employee;

    public function __construct($computer_name, $computer_model, $service_tag, $status, $id_employee){
        $this->computer_name = $computer_name;
        $this->computer_model = $computer_model;
        $this->service_tag = $service_tag;
        $this->status = $status;
        $this->id_employee = $id_employee;
    }

    public function getComputer_name(){
        return $this->computer_name;
    }

    public function setComputer_name($computer_name): void{
        $this->computer_name = $computer_name;
    }

    public function getComputer_model(){
        return $this->computer_model;
    }

    public function setComputer_model($computer_model): void{
        $this->computer_model = $computer_model;
    }

    public function getService_tag(){
        return $this->service_tag;
    }

    public function setService_tag($service_tag): void{
        $this->service_tag = $service_tag;
    } 

    public function getStatus(){
        return $this->service_tag;
    }

    public function setStatus($service_tag): void{
        $this->service_tag = $service_tag;
    }

    public function getId_employee(){
        return $this->id_employee;
    }

    public function setId_employee($id_employee): void{
        $this->id_employee = $id_employee;
    }
}