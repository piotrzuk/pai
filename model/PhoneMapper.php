<?php

require_once 'Phone.php';
require_once __DIR__.'/../Database.php';

class PhoneMapper{ 
    private $database;

    public function __construct(){
        $this->database = new Database();
    }

    public function getPhones(){
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM phones LEFT JOIN employess ON employess.id_employee = phones.id_employee;');

            $stmt->execute();

            $phones = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $phones;
        }
        catch(PDOException $e) {
            die();
        }
    }
    
    public function getEmployess(){
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM employess;');

            $stmt->execute();
            $employee = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $employee;
        }
        catch(PDOException $e) {
            die();
        }
    }

    public function setPhone(Phone $phone) :void{
        $imei = $phone->getImei();
        $phone_number = $phone->getPhone_number();
        $phone_model = $phone->getPhone_model();
        $status = $phone->getStatus();
        $id_employee = $phone->getId_employee();

        try{
            $stmt = $this->database->connect()->prepare('INSERT INTO phones (imei, phone_number, phone_model, status, id_employee) VALUES (:imei, :phone_number, :phone_model,:status, :id_employee);');
                
            $stmt->bindParam(':imei', $imei, PDO::PARAM_STR);
            $stmt->bindParam(':phone_number', $phone_number, PDO::PARAM_INT);
            $stmt->bindParam(':phone_model', $phone_model, PDO::PARAM_STR);
            $stmt->bindParam(':status', $status, PDO::PARAM_STR);
            $stmt->bindParam(':id_employee', $id_employee, PDO::PARAM_INT);
            $stmt->execute();
        }

        catch(PDOException $e) {
            die();
        }
    }

    public function delete(int $id_phone): void{
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM phones WHERE id_phone = :id_phone;');
            $stmt->bindParam(':id_phone', $id_phone, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $e) {
            die();
        }
    }
}