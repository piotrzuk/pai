<?php

class Phone{
    private $id_phone;
    private $imei;
    private $id_employee;
    private $phone_number;
    private $phone_model;
    private $status;

    public function __construct($imei, $phone_number, $phone_model, $status, $id_employee){
        $this->imei = $imei;
        $this->phone_number = $phone_number;
        $this->phone_model = $phone_model;
        $this->status = $status;
        $this->id_employee = $id_employee;
    }

    public function getId_phone(){
        return $this->id_phone;
    }

    public function setId_phone($id_phone): void{
        $this->id_phone = $id_phone;
    }

    public function getImei(){
        return $this->imei;
    }

    public function setImei($imei): void{
        $this->imei = $imei;
    }

    public function getId_employee(){
        return $this->id_employee;
    }

    public function setId_employee($id_employee): void{
        $this->id_employee = $id_employee;
    }

    public function getPhone_number(){
        return $this->phone_number;
    }

    public function setPhone_number($phone_number): void{
        $this->phone_number = $phone_number;
    }

    public function getPhone_model(){
        return $this->phone_model;
    }

    public function setPhone_model($phone_model): void{
        $this->phone_model = $phone_model;
    }
    public function getStatus(){
        return $this->status;
    }

    public function setStatus($status): void{
        $this->status = $status;
    }
}