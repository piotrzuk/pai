<?php

require_once 'Computer.php';
require_once __DIR__.'/../Database.php';

class ComputerMapper{ 
    private $database;

    public function __construct(){
        $this->database = new Database();
    }

    public function getComputers(){
        try {
            $stmt = $this->database->connect()->prepare('SELECT employess.*, computers.*, employess.id_employee FROM computers LEFT JOIN employess ON employess.id_employee = computers.id_employee;');

            $stmt->execute();

            $computer = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $computer;
        }
        catch(PDOException $e) {
            die();
        }
    }
    
    public function getEmployess(){
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM employess;');

            $stmt->execute();
            $employee = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $employee;
        }
        catch(PDOException $e) {
            die();
        }
    }

    public function setComputer(Computer $computer) :void{
        $computer_name = $computer->getComputer_name();
        $computer_model = $computer->getComputer_model();
        $service_tag = $computer->getService_tag();
        $status = $computer->getStatus();
        $id_employee = $computer->getId_employee();

        try{
            $stmt = $this->database->connect()->prepare('INSERT INTO computers (computer_name, computer_model, service_tag, status, id_employee) VALUES (:computer_name, :computer_model, :service_tag,:status, :id_employee);');
                
            $stmt->bindParam(':computer_name', $computer_name, PDO::PARAM_STR);
            $stmt->bindParam(':computer_model', $computer_model, PDO::PARAM_STR);
            $stmt->bindParam(':service_tag', $service_tag, PDO::PARAM_STR);
            $stmt->bindParam(':status', $status, PDO::PARAM_STR);
            $stmt->bindParam(':id_employee', $id_employee, PDO::PARAM_INT);
            $stmt->execute();
        }

        catch(PDOException $e) {
            die();
        }
    }

    public function delete(int $id_computer): void{
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM computers WHERE id_computer = :id_computer;');
            $stmt->bindParam(':id_computer', $id_computer, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $e) {
            die();
        }
    }
        
    
}