<?php

require_once 'Employee.php';
require_once __DIR__.'/../Database.php';

class EmployeeMapper{ 
    private $database;

    public function __construct(){
        $this->database = new Database();
    }
    
    public function getEmployess(){
        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM employess;');

            $stmt->execute();
            $employee = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $employee;
        }
        catch(PDOException $e) {
            die();
        }
    }

    public function setEmployee(Employee $employee) :void{
        $name = $employee->getName();
        $surename = $employee->getSurename();

        try{
            $stmt = $this->database->connect()->prepare('INSERT INTO employess (name, surename) VALUES (:name, :surename);');
                
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':surename', $surename, PDO::PARAM_STR);
            $stmt->execute();
        }

        catch(PDOException $e) {
            die();
        }
    }

    public function delete(int $id_employee): void{
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM employess WHERE id_employee = :id_employee;');
            $stmt->bindParam(':id_employee', $id_employee, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $e) {
            die();
        }
    }
        
    
}