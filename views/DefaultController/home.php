<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html'); ?>

<body>
<?php if(isset($_SESSION) && !empty($_SESSION)): ?>
<?php include(dirname(__DIR__).'/header.html') ?>

<?php 
else:
  header("Location: ?page=login");
  exit; 
endif;?>
</body>
</html>