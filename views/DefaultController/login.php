<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html') ?>

<body>
<div id = login>
<h1>Logowanie</h1>

<?php if(isset($message)): ?>
    <?php foreach($message as $item): ?>
        <div><?= $item ?></div>
    <?php endforeach; ?>
<?php endif; ?>

<form action="?page=login" method="POST">

    <input name="login" class="form-control" placeholder="wprowadź login" required/>
    <br>
    <input name="password" class="form-control" placeholder="wprowadź hasło" type="password" required/>
    <br>
    <input type="submit" value="Zaloguj się" class="btn btn-primary"/>
</div>

</form>

</body>
</html>