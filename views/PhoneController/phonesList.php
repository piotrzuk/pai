<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html') ?>

<body>
<?php if(isset($_SESSION) && !empty($_SESSION)): ?>

<?php include(dirname(__DIR__).'/header.html') ?>

<table class="table table-striped">
	<thead>
        <tr>
			<th>IMEI</th>
			<th>Numer Telefonu</th>
			<th>Model telefonu</th>
			<th>Status</th>
			<th>Użytkownik</th>
			<th>Usuń </th>
		</tr>
	</thead>
	<tbody>
		
		<?php
			foreach ($phones as $phone) {
				echo "<tr>
				<td>{$phone['imei']}</td>
				<td>{$phone['phone_number']}</td>
				<td>{$phone['phone_model']}</td>
				<td>{$phone['status']}</td>";
				if($phone['name']){				
					echo "<td>{$phone['name']}{$phone['surename']}</td>";}
				else{
					echo "<td> wolny </td>";

				};
				
				?>
				<td>
				<button class="btn btn-danger" type="button" onclick="deletePhone(<?php echo $phone['id_phone']; ?>)">
						<i class="material-icons">delete_forever</i>
				</button>
				</td>
				</tr>
		<?php 
			}
		?>
	</tbody>
</table>
<button class="btn btn-primary" type="button" onclick="location='?page=phoneAdd';">Dodaj telefon</button>
<?php 
else:
  header("Location: ?page=login");
  exit;
endif;?>
</body>
</html>