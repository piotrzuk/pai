<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html') ?>

<body>
<?php if(isset($_SESSION) && !empty($_SESSION)): ?>

<?php include(dirname(__DIR__).'/header.html') ?>

<div class="addComp">
<h1>Dodaj Uzytkownika</h1>
<form action="?page=addUsers" method="POST">
    <input class="form-control" name="login" placeholder="Login" required/>
    <input class="form-control" name="email" placeholder="Email" type="email" required/>
    <input class="form-control" name="password" placeholder="Haslo" type="password" required/>
    <label for="exampleFormControlSelect1">Rola użytkownika</label>
    <select class="form-control" name="id_role">
      <?php 
        foreach ($roles as $role) {
          echo "
          <option value={$role['id_role']}>{$role['rola']}</option>
          ";
        };
      ?>
    </select>
    <input type="submit" value="Zapisz" class="btn btn-primary"/>
    <a href="?page=admin" class="btn btn-primary">Wróć do listy</a>
</div>


<?php 
else:
  header("Location: ?page=login");
  exit;
endif;?>
</body>
</html>