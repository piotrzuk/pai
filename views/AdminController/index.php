<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html') ?>

<body>
<?php if(isset($_SESSION) && !empty($_SESSION)): ?>

<?php include(dirname(__DIR__).'/header.html') ?>

<?php if(isset($message)): ?>
                <?php foreach($message as $item): ?>
                    <div><?= $item ?></div>
                <?php endforeach; ?>
            <?php endif; ?>
            
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>Email</th>
                        <th>Rola</th>
                        <th>Akcje</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?=$user->getLogin(); ?></td>
                        <td><?=$user->getEmail(); ?></td>
                        <td><?=$user->getId_Role(); ?></td>
                        <td> - </td>
                    </tr>
            </tbody>
            <tbody class="users-list"></tbody>
            </table>

            <button class="btn btn-primary" type="button" onclick="getUsers()">Wszyscy użytkownicy</button>
            <button class="btn btn-primary" type="button" onclick="location='?page=addUsers';">Dodaj użytkownika</button>


<?php 
else:
  header("Location: ?page=login");
  exit;
endif;?>
</body>
</html>