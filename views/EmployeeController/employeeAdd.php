<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html') ?>

<body>
<?php if(isset($_SESSION) && !empty($_SESSION)): ?>
<?php include(dirname(__DIR__).'/header.html') ?>

<div class="addComp">
<h1>Dodaj pracownika do listy</h1>
<form action="?page=employeeAdd" method="POST">
    <input class="form-control" name="name" placeholder="Imie" required/>
    <input class="form-control" name="surname" placeholder="Nazwisko" required/>

    <input type="submit" value="Zapisz" class="btn btn-primary"/>
    <a href="?page=employeeList" class="btn btn-primary">Wróć do listy</a>
</div>


<?php 
else:
  header("Location: ?page=login");
  exit;
endif;?>
</form>
</body>
</html>