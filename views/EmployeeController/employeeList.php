<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html') ?>

<body>
<?php if(isset($_SESSION) && !empty($_SESSION)): ?>

<?php include(dirname(__DIR__).'/header.html') ?>

<table class="table table-striped">
	<thead>
        <tr>
			<th>Imię</th>
			<th>Nazwisko</th>
			<th>Usuń </th>
		</tr>
	</thead>
	<tbody>
		
		<?php
			foreach ($employess as $employee) {
				echo "<tr>
                    <td>{$employee['name']}</td>
                    <td>{$employee['surename']}</td>";
		?>
				<td>
				<button class="btn btn-danger" type="button" onclick="deleteEmployee(<?php echo $employee['id_employee']; ?>)">
						<i class="material-icons">delete_forever</i>
				</button>
				</td>
			</tr>
		<?php 
			};
		?>
			
	</tbody>
</table>
<button class="btn btn-primary" type="button" onclick="location='?page=employeeAdd';">Dodaj pracownika</button>
<?php 
else:
  header("Location: ?page=login");
  exit;
endif;?>
</body>
</html>