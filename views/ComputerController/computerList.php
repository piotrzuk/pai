<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html') ?>

<body>
<?php if(isset($_SESSION) && !empty($_SESSION)): ?>

<?php include(dirname(__DIR__).'/header.html') ?>

<table class="table table-striped">
	<thead>
        <tr>
			<th>ID komputera</th>
			<th>Nazwa komputera</th>
			<th>Model komputer</th>
			<th>Service tag</th>
			<th>Status</th>
			<th>Użytkownik</th>
			<th>Usuń </th>
		</tr>
	</thead>
	<tbody>
		
		<?php
			foreach ($computers as $computer) {
				echo "<tr>
				<td>{$computer['id_computer']}</td>
				<td>{$computer['computer_name']}</td>
				<td>{$computer['computer_model']}</td>
				<td>{$computer['service_tag']}</td>
				<td>{$computer['status']}</td>";
				if($computer['name']){				
					echo "<td>{$computer['name']} {$computer['surename']}</td>";}
				else{
					echo "<td> brak </td>";
				};
				
				?>
				<td>
				<button class="btn btn-danger" type="button" onclick="deleteComputer(<?php echo $computer['id_computer']; ?>)">
						<i class="material-icons">delete_forever</i>
				</button>
		<?php 
			echo "
			</td>
			</tr>";
			}
		?>
	</tbody>
</table>
<button class="btn btn-primary" type="button" onclick="location='?page=computerAdd';">Dodaj komputer</button>
<?php 
else:
  header("Location: ?page=login");
  exit;
endif;?>
</body>
</html>