<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__).'/head.html') ?>

<body>
<?php if(isset($_SESSION) && !empty($_SESSION)): ?>
<?php include(dirname(__DIR__).'/header.html') ?>

<div class="addComp">
<h1>Dodaj komputer do listy</h1>
<form action="?page=computerAdd" method="POST">
    <input class="form-control" name="computer_name" placeholder="Nazwa komputera" required/>
    <input class="form-control" name="computer_model" placeholder="Model komputera" required/>
    <input class="form-control" name="service_tag" placeholder="Service tag" required/>
    <label for="formControlSelect1">Wybierz status</label>
	  <select class="form-control" name="status">
        <option value="Zajęty"> Zajęty </option>
        <option value="Wolny"> Wolny </option>
        <option value="Serwis"> Serwis </option>
        <option value="Wycofany"> Wycofany </option>
    </select>

    <label for="formControlSelect1">Pracownik korzystający z komputera</label>
		<select class="form-control" name="id_employee">
      <option value="NULL"> Wybierz </option>
      <?php 
        foreach ($employess as $employee) {
          echo"		
          <option value={$employee['id_employee']}>{$employee['name']} {$employee['surename']}</option>
          ";
        }; 
      ?>
    </select>
    <input type="submit" value="Zapisz" class="btn btn-primary"/>
    <a href="?page=computerList" class="btn btn-primary">Wróć do listy</a>
</div>


<?php 
else:
  header("Location: ?page=login");
  exit;
endif;?>
</form>
</body>
</html>